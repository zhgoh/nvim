if (has("termguicolors"))
  "set termguicolors
endif

" TODO: Take a look at how to handle the different OS
if has("win32")
  set background=light
else
  let myterm = $TERM
  if myterm=~'linux'
    " Arch TTY
    set background=dark
  elseif myterm=~'vtpcon'
    " Windows Terminal
    set background=light
  elseif empty($DISPLAY)
    "FreeBSD vt
    set background=dark
  else
    " Not in TTY
    set background=light
  endif
endif

" ---------- General settings ----------
set autochdir                     " Set the working directory to wherever the open file lives
set autoindent                    " Good auto indentation
set backspace=indent,eol,start    " Backspace behaves as expected
set clipboard=unnamedplus         " Global copy/paste
set cmdheight=2                   " More space for displaying message
set conceallevel=0                " So that I can see `` in markdown files
set cursorline                    " Enable highlighting of current line
set encoding=utf-8                " The encoding displayed
set expandtab                     " Convert tabs to space
set fileencoding=utf-8            " The encoding written to file
set foldmethod=manual             " Set indentation method to go by indentation level
set formatoptions-=cro            " Stop newline continution of comments
" set hidden                      " Hide buffer (file) instead of abandoning when switching to another buffer
" set hlsearch                    " Highlight search results
set incsearch                     " Highlight search while typing
set ignorecase                    " Ignore case when searching
set iskeyword+=-                  " Treat dash separated words as a word text object"
set laststatus=0                  " Always display the status line
set nobackup                      " This is recommended by coc
set nocompatible                  " Enable vim to not be compatible with vi
set noerrorbells                  " Prevent Vim from beeping
" set nofoldenable                  " Disable folding
" set noshowmode                  " We don't need to see things like -- INSERT -- anymore
set nostartofline                 " Cursor do not go to beginning of line when jumping using g key
set nowrap                        " Don't wrap lines when it gets too long
set nowritebackup                 " This is recommended by coc
set number                        " Show line number
set path+=**                      " Search down into subfolders 
set pumheight=10                  " Makes popup menu smaller
set ruler                         " Display cursor position
set scrolloff=3                   " Display at least 3 lines around cursor for scrolling
set shiftwidth=2                  " Change the number of space characters inserted for indentation
" set showtabline=2               " Always show tabs 
set smartcase                     " Use case sensitive search when search has UpperCase.
set smartindent                   " Good indentation
set smarttab                      " Makes tabbing smarter will realize you have 2 vs 4
set softtabstop=2
set splitbelow                    " More natural split opening
set splitright                    " will split to right and bottom when opening new splits
set tabstop=2                     " Change the number of space characters inserted for indentation
set t_Co=256                      " Support 256 colors
" set timeoutlen=100              " By default timeoutlen is 1000 ms
set title                         " Update the title of your window or terminal   
set updatetime=300                " Faster completion
set visualbell                    " Prevent Vim from beeping
set wildmenu                      " Show file options above the command line

syntax enable                     " Enable syntax highlighting
"colorscheme onedark               " Use onedark scheme loaded by plugin manager
colorscheme gruvbox
filetype indent plugin on         " Enable file specific behavior like syntax highlighting and indentation 

au! BufWritePost $MYVIMRC source %      " auto source when writing to init.vm alternatively you can run :source $MYVIMRC

" Don't offer to open certain files/directories
set wildignore+=*.bmp,*.gif,*.ico,*.jpg,*.png
set wildignore+=*.pdf,*.psd
set wildignore+=\\node_modules\\*,/tmp/*,\\tmp\\*
set wildignore+=*.class,*.bin,*.exe,*.dll,*.so,*.zip

" Set my own custom status line
" Color can be gotton from :so $VIMRUNTIME/syntax/hitest.vim
set statusline=                 " Clear the old status line
set statusline+=%#Search#
set statusline+=%m              " Modified flag
set statusline+=%y              " File type
set statusline+=%r              " Read only flag
"set statusline+=%1=             " Divider
"set statusline+=%#WildMenu#
set statusline+=%F              " File name
set statusline+=%=              " Right side settings
" set statusline+=%#DiffAdd#
set statusline+=%c              " Column number
set statusline+=:
set statusline+=%l              " Current line number
set statusline+=/
set statusline+=%L              " File line number
set statusline+=[%n]            " Buffer number

" in makefiles, don't expand tabs to spaces, since actual tab characters are
" needed, and have indentation at 8 chars to be sure that all indents are tabs
" (despite the mappings later):
autocmd FileType make set noexpandtab shiftwidth=8 softtabstop=0

" Reset it back when using C file, in case editing makefile and going back to c source
autocmd FileType c set noexpandtab shiftwidth=2 softtabstop=2

" disables automatic commenting on newline
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o     

" when to activate neomake
call neomake#configure#automake('nrw', 50)

" which linter to enable for Python source file linting
let g:neomake_python_enabled_makers = ['pylint']
