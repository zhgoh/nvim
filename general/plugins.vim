" --- Vim Plug
" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin($PLUGDIR)
  Plug 'tpope/vim-surround'
  Plug 'townk/vim-autoclose'
  Plug 'tpope/vim-commentary'
  Plug 'ervandew/supertab'
  Plug 'sheerun/vim-polyglot'

  Plug 'neomake/neomake'
  " If you have nodejs and yarn
  Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }

  " Themes
  Plug 'morhetz/gruvbox'
call plug#end()
