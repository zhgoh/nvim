if has ('win32')
  let $NVIMDIR = '~/AppData/Local/nvim/'
else
  let $NVIMDIR = '~/.config/nvim/'
endif

let $GENERALDIR    = $NVIMDIR.'general/'
let $PLUGDIR       = $NVIMDIR.'plugged/'
let $KEYDIR        = $NVIMDIR.'keys/'
let $SETTINGSVIM   = $GENERALDIR.'settings.vim'
let $PLUGVIM       = $GENERALDIR.'plugins.vim'
let $KEYVIM        = $KEYDIR.'mappings.vim'

source $PLUGVIM
source $KEYVIM
source $SETTINGSVIM
