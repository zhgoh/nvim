# Neovim Configuration

1. init.vim is to be put in ~/.config/ or ~/AppData/Local/
2. Follow Vim Plug README.md to set up vim plug
3. Do :PlugInstall in nvim

## Notes

vimrc referenced from here
https://www.chrisatmachine.com/Neovim/02-vim-general-settings/
