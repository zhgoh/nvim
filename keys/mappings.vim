" set leader key
" let g:mapleader = "\<Space>"

" ---------- Keymap settings ----------
" Easier split navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Easier ESC key
inoremap jk <ESC>
inoremap kj <ESC>
nnoremap <C-c> <ESC>

" Easy to uppercase the word
inoremap <c-u> <ESC>viwUi
nnoremap <c-u> viwU<ESC>

" Will keep selection highlighted when indenting
vnoremap < <gv
vnoremap > >gv

" TAB in general mode will move to text buffer
nnoremap <TAB> :bnext<CR>
" SHIFT-TAB will go back
nnoremap <S-TAB> :bprevious<CR>

" Show autocompletion when pressing tab
inoremap <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"

" Show autocompletion when pressing C-j
inoremap <expr> <C-j> ("\<C-n>")
inoremap <expr> <C-k> ("\<C-p>")

" Use alt + hjkl to resize windows
nnoremap <M-j>    :resize -2<CR>
nnoremap <M-k>    :resize +2<CR>
nnoremap <M-h>    :vertical resize -2<CR>
nnoremap <M-l>    :vertical resize +2<CR>

" Leader + / will toggle comments
noremap <leader>/ :Commentary<CR>

" Auto load my init.vim file again
nnoremap <leader>r :source $MYVIMRC<CR>
nnoremap <leader>m :edit $MYVIMRC<CR>

" Clear highlight on ESC
nnoremap <ESC> :nohlsearch<CR><ESC>

" Easier to fuzzy open files
nnoremap <leader>p :edit **/*
nnoremap <leader>v :vsplit **/*
nnoremap <leader>s :split **/*

" `gf` opens file under cursor in a new vertical split
nnoremap gf :vertical wincmd f<CR>

" Change to current file dir
" noremap <leader>c :lcd %:p:h<cr>

